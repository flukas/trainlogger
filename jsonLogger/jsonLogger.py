import urllib.request
import json
import datetime
import time
import atexit

f = open(datetime.date.today().isoformat()+ " - gpx log", "a")
pastcontents = ""

gpxStart="""
<?xml version="1.0" encoding="UTF-8" standalone="no" ?>

<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="jsonLogger" version="1.1" >
  <trk>
    <trkseg>
"""

gpxEnd = """
    </trkseg>
  </trk>
</gpx>
"""

ptHeader='<trkpt lat="{lat}" lon="{lon}">'
speedForm='\t<speed>{}</speed>'
altitudeForm='<ele>\t{}</ele>'
temperatureForm='\t<temperature>{}</temperature>'
timeForm='\t<time>{}</time>'
ptEnd='</trkpt>'

class expando(object): pass


class Element:
	elementType = ""  # identifier
	formatter = ""  # xml formatter
	otherName = ""  # name in 

def appendElement(toAppendTo,elementType, datasource, **kwargs):
	"""maybe sometime"""
	if elementName is not None and elementName in datasource:
		val = datasource.get(config[elementType])
		toAppendTo.append(elementFormatter.format(kwargs))



# ČD
# {"gpsLat":50.031482599999997,"gpsLng":15.756419299999999,"speed":0,"delay":0,"altitude":228.19999999999999,"temperature":null}
setCD=expando()
setCD.latName="gpsLat"
setCD.lonName="gpsLng"
setCD.tempName="temperature"
setCD.speedName="speed"
setCD.altName="altitude"
setCD.url="http://cdwifi.cz/portal/api/vehicle/realtime?distances=km&temperatures=c"

# ÖBB
# {"Latitude":"49.248035","Longitude":"16.672327"}
setOEBB=expando()
setOEBB.latName="Latitude"
setOEBB.lonName="Longitude"
setOEBB.tempName=None
setOEBB.speedName=None
setOEBB.altName=None
setOEBB.url="http://railnet.oebb.at/api/gps"


config = setCD
config.sleepTime = 1
config.sleppTimeAutoTune = True
f.write(gpxStart)

while True:

	contents = urllib.request.urlopen(config.url).read()
	if contents == pastcontents:
		continue
	else:
		pastcontents = contents

		# parse
		parsed = json.loads(contents.decode('UTF-8'))
		# construct point
		ptMembers = [ptHeader.format(lat=parsed.get(config.latName), lon=parsed.get(config.lonName))]

		# time
		ptMembers.append(timeForm.format(time=datetime.datetime.utcnow().replace(microsecond=0).isoformat()))

		# temperature
		if config.tempName is not None and config.tempName in parsed:
			ptMembers.append(temperatureForm.format(temperature=parsed.get(config.tempName)))

		# altitude
		if config.altName is not None and config.altName in parsed:
			ptMembers.append(altitudeForm.format(altitude=parsed.get(config.altName)))

		# speed
		if config.speedName is not None and config.speedName in parsed:
			tmp = parsed.get(config.speedName)
			ptMembers.append(speedForm.format(speed=parsed.get(config.speedName)))

		#end
		ptMembers.append(ptEnd)
		ptMembers.append("")  # ensures \n after last element
		trackPoint = '\n'.join(ptMembers)
		f.write(trackPoint)
		f.flush()

		time.sleep(config.sleepTime)


